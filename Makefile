webapp:
	python3 app.py

worker:
	celery -A worker worker --beat -E --loglevel=debug

server: webapp worker