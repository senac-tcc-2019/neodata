from passlib.hash import pbkdf2_sha256
from neodata.settings import db
from neodata.database import User, Project, Plan

# criar usuário Admin

user = User(name="Frederico Schmitt Kremer", 
            email="fred.s.kremer@gmail.com", 
            password=pbkdf2_sha256.hash("atbiotec2013"),
            is_admin=True,
            is_activated=True,
            stripe_id="cus_FwhP4wP1mjTgUA")

db.session.add(user)
db.session.commit()

# criar projetos de exemplo

project_pg = Project(
                    user_id = user.id,
                    name = "Forecasting 'Favorita' (PostgeSQL)",
                    db_name = "o0hpxvmvl2w2f5l9",
                    db_host = "ml3k5189rhw3worn.cbetxkdyhwsb.us-east-1.rds.amazonaws.com",
                    db_user = "lf9tx98olbamgnom",
                    db_pass = "g289yw032top8qa7",
                    db_port = 5432,
                    db_engine = "postgres",
                    sql_query = "SELECT sku, date, y FROM sales",
                    status = "queued"
                )

db.session.add(project_pg)
db.session.commit()

# criar planos 

plan_monthly = Plan(product_stripe_id="prod_FyuD2XGPBCnhuu", plan_stripe_id="plan_FyuEisaVVOAN2w", period="monthly",value=98.)
plan_yearly = Plan(product_stripe_id="prod_FyuD2XGPBCnhuu", plan_stripe_id="plan_FyuESzYQwbqtQd", period="yearly",value=998.95)

db.session.add(plan_monthly)
db.session.add(plan_yearly)
db.session.commit()
