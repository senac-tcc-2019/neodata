# Neodata

## Uma ferramenta para análise de séries temporais 
## Discente

Frederico Schmitt Kremer


## Orientador 

MSc. Angelo Gonçalves da Luz

## Acessar o servidor

A aplicação pode ser acessada através do endereço [http://104.197.148.23/](http://104.197.148.23/). A conta de administrador através das credenciais:
- **Email**: `fred.s.kremer@gmail.com`
- **Password**: `1234` 

## Realizando o *deploy*
  
  - Instalar as dependências:
	  `$ sudo apt install python3-pip mysql-server mysql-client redis`
  - Instalar as bibliotecas no Python:
	  `$ pip3 install -r requirements.txt`
	  
  - Criar banco de dados no MySQL (ex: `neodata`);
  - Editar arquivo `.env` ou configurar no sistema as seguintes variáveis de ambiente:
	  - `SQLALCHEMY_DATABASE_URI`: URI de acesso ao banco de dados principal;
	  - `STRIPE_API_KEY_PUBLIC`: Chave pública para acesso à API do Stripe;
	  - `STRIPE_API_KEY_SECRET`: Chave privada para acesso à API do Stripe;
	  - `MAIL_SERVER`: Servidor de SMTP que será usado para o serviço de email;
	  - `MAIL_PORT`: Porta do servidor de SMTP;
	  - `MAIL_USERNAME`: Usuário do servidor de SMTP;
	  - `MAIL_PASSWORD`: Senha do usuário de SMTP;
	  - `NEODATA_HOSTNAME`: URL para o *host*;
	  - `NEODATA_PORT`: Porta que o servidor deve se conectar;

## Executar
### Executando "manualmente"

`$ make -j2 server`

### Executando como um *daemon*

- Instalar o Supervisor:
	`sudo apt install supervisor`
- Criar um arquivo de configuração de processo para o Supervisor. Exemplo de configuração:
  ```
  [program:neodata_webapp]
  command=bash -c 'make webapp'
  directory=/home/frederico/neodata/
  umask=022
  priority=999
  autostart=true
  autorestart=true
  startsecs=10
  startretries=10
  exitcodes=0
  stopsignal=TERM
  stopwaitsecs=10
  stopasgroup=true
  killasgroup=true
  user=root
  [program:neodata_worker]
  command=bash -c 'make worker'
  directory=/home/frederico/neodata/
  umask=022
  priority=999
  autostart=true
  autorestart=true
  startsecs=10
  startretries=10
  exitcodes=0
  stopsignal=TERM
  stopwaitsecs=10
  stopasgroup=true
  killasgroup=true
  user=frederico

  [group:neodata]
  programs=neodata_webapp,neodata_worker
  ```
 - Reinicializar o Supervisor:
   `$ sudo supervisorctl reload`