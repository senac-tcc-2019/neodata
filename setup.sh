#!/usr/bin/env bash

while read requirement; do 
    pip3 install $requirement; 
done < requirements.txt

if [ -d migrations ]; then 
    rm -r migrations/ 
fi

flask db init
flask db migrate