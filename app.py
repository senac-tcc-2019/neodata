#!/usr/bin/python3
# -*- coding: utf-8 -*- 

from flask import flash, render_template, request, url_for, redirect, jsonify, abort
from flask_login import login_user, logout_user, current_user
from neodata.settings import app, db, login_manager, login_required
from neodata.database import User, Project
from passlib.hash import pbkdf2_sha256
import os

@login_manager.user_loader
def load_user(user_id):
    user = User.query.filter_by(id=user_id).first()
    return user

@app.route("/")
def index():
    return render_template("index.html", static_url_path='/static')

@login_manager.unauthorized_handler
def unauthorized():
    flash('Efetue login para acessar este recurso', 'error')
    return render_template("login.html", static_url_path='/static')

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0', port=int(os.environ.get('NEODATA_PORT', '5000')))
