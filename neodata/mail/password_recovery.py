from flask_mail import Message
from neodata.settings import app

def recovery_password_email(username, token):
    return f'''
    <html>
    <body>
    <p>Olá {username} ^^</p>

    <p>Segue abaixo o link para a recuperação da sua senha :D</p>
    <br>
    http://{app.config["NEODATA_HOSTNAME"]}/recovery/{token}
    </body>
    </html>
    '''

def create_recovery_password_email_message(username, usermail, token):
    message = Message(subject='Neodata: password recovery', 
                      recipients=[usermail],
                      html=recovery_password_email(username, token),
                      sender='neodata.forecasting@gmail.com')
    return message