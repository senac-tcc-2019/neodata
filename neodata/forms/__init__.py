from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Email, Length, AnyOf

class RegistrationForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(message="User's name must be provided")])
    email = StringField('email', validators=[DataRequired(message="User's e-mail must be provided"), Email(message="A valid e-mail must be provided")])
    password = StringField('password', validators=[DataRequired(message="A password must be provided"), Length(message="the minimum length is 6 chars", min=6)])

class LoginForm(FlaskForm):
    email = StringField('email', validators=[DataRequired(message="User's e-mail must be provided")])
    password = StringField('password', validators=[DataRequired(message="User's password must be provided")])

class ProjectForm(FlaskForm):
    project_name = StringField('project_name', validators=[DataRequired()])
    db_host = StringField('db_host', validators=[DataRequired(message="Database host must be provided")])
    db_name = StringField('db_name', validators=[DataRequired(message="Database name must be provided")])
    db_user = StringField('db_user', validators=[DataRequired(message="Database user must be provided")])
    db_pass = StringField('db_pass', validators=[DataRequired(message="Database password must be provided")])
    db_port = StringField('db_port', validators=[DataRequired(message="Database port must be provided")])
    db_engine = StringField('db_engine', default='postgres')
    sql_query = StringField('sql_query', validators=[DataRequired(message="SQL query must be provided")])