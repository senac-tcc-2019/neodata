from dotenv import load_dotenv
from datetime import datetime
import stripe

def update_subscription_status():
    from neodata.settings import db
    from neodata.database import User, Subscription
    for user in User.query.all():
        user.is_activated = False
        subscriptions = Subscription.query.filter_by(user_id=user.id).all()
        for subscription in subscriptions:
            subscription_data = stripe.Subscription.retrieve(subscription.stripe_id)
            subscription.status = subscription_data.status
            subscription.current_period_end = datetime.fromtimestamp(subscription_data.current_period_end)
            db.session.commit()
            period_end = datetime.fromtimestamp(subscription_data.current_period_end)
            if (subscription.status == 'active') or (datetime.now() < period_end) or (user.is_admin):
                user.is_activated = True
        db.session.commit()
    