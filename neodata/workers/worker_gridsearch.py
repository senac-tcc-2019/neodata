from sqlalchemy import create_engine
from neodata.settings import db
from neodata.database import Project, Report, ReportData, ProphetSettings
from dotenv import load_dotenv
from datetime import timedelta
from fbprophet import Prophet
from sklearn.metrics import mean_squared_error
import numpy as np
import pandas as pd
import datetime
import calendar
import os
import itertools 
import tqdm
import json

load_dotenv()

def grid_search():
    projects = Project.query.all()
    
    settings_grid = {
        'seasonality_mode': ['additive', 'multiplicative'],
        'yearly_seasonality': [1, 10, 50],
        'weekly_seasonality': [1, 10, 50]
    }

    for project in projects:

        connection = get_project_connection(project)
        if not connection:
            continue

        for setting_list in tqdm.tqdm(list(itertools.product(*settings_grid.values()))):
            settings = dict(zip(settings_grid.keys(), setting_list))
            forecaster = run_forecasting(project, connection, settings=settings)

            for sku, sku_data, forecast_data in forecaster:

                best_median_rmse = None

                rmse_list = []

                evaluation_size = int(sku_data.shape[0] // 2) + 1

                real_data = sku_data.head(evaluation_size)[['date', 'y']]
                forecast_data = forecast_data[['date', 'y']]
                forecast_data =  forecast_data[(forecast_data['date'] >= real_data['date'].min()) & (forecast_data['date'] <= real_data['date'].max())]

                real_data_values = []
                forecast_data_values = []

                for _, row in real_data.iterrows():
                    if _ == evaluation_size:
                        break
                    real_data_values.append(row['y'])
                    forecast_data_values.append(forecast_data[(forecast_data['date'] == row['date'])]['y'].iloc[0])

                rmse = mean_squared_error(real_data_values, forecast_data_values)
                rmse_list.append(rmse)
                median_rmse = np.median(rmse_list)

                if (best_median_rmse == None) or (median_rmse < best_median_rmse):

                    best_median_rmse = median_rmse
                    best_setting = settings
                    prophet_best_settings = ProphetSettings.query.filter_by(project_id=project.id, sku=sku).first()

                    if prophet_best_settings:
                        prophet_best_settings.rmse = float(best_median_rmse)
                        prophet_best_settings.settings_json = json.dumps(best_setting)

                    else:
                        prophet_best_settings = ProphetSettings()
                        prophet_best_settings.sku = sku
                        prophet_best_settings.project_id = project.id
                        prophet_best_settings.rmse = float(best_median_rmse)
                        prophet_best_settings.settings_json = json.dumps(best_setting)                    
                        db.session.add(prophet_best_settings)

                    db.session.commit()
    
def run_forecasting(project, connection, settings):
    for sku, sku_data in get_project_data(project, connection):
        yield sku, sku_data, run_prophet(sku, sku_data, settings=settings)

def get_project_connection(project):
    if project.db_engine == 'mysql':
        connector_uri = 'mysql+pymysql'
    elif project.db_engine == 'postgres':
        connector_uri = 'postgresql+psycopg2'
    try:
        connection = create_engine(f'{connector_uri}://{project.db_user}:{project.db_pass}'
                                   f'@{project.db_host}/{project.db_name}').connect()
        return connection
    except:
        return None

def get_project_data(project, connection):
    data = pd.read_sql(project.sql_query, connection)

    final_daily_df = pd.DataFrame()

    grouper = data['date'].dt.to_period('D')

    for p, period_df in data.groupby(grouper):
        for  sku, sku_df in period_df.groupby(by='sku'):
            final_daily_df = final_daily_df.append({
                'date': p.end_time,
                'y': sku_df['y'].sum(),
                'sku': sku
            }, 
            ignore_index=True)

    final_daily_df['ds'] = final_daily_df['date']
    return final_daily_df.groupby(by="sku")

def run_prophet(sku, skus_data, settings=None):

    skus_data['ds'] = skus_data['date']
    skus_data['y'] = skus_data['y']

    group_df = skus_data[skus_data['sku']==sku]

    if not settings:
        m = Prophet()
    else:
        m = Prophet(**settings)

    evaluation_size = int(skus_data.shape[0] // 2)

    m.fit(group_df.head(int(group_df.shape[0])-evaluation_size))
    future = m.make_future_dataframe(periods=skus_data.shape[0] - evaluation_size + 31*6)
    forecast = m.predict(future)
    forecast['date'] = forecast['ds']
    forecast['y'] = forecast['yhat']
    forecast['sku'] = sku

    return forecast


def save_forecasting_to_database(project, sku, real, forecast):
    report = Report()
    report.project_id = project.id
    report.date = datetime.datetime.now()
    db.session.add(report)
    db.session.commit()

    for _, row in real.iterrows():
        report_data = ReportData()
        report_data.report_id = report.id
        report_data.date = str(row.date)
        report_data.value = row.y
        report_data.sku = sku
        report_data.is_prediction = False
        db.session.add(report_data)
        db.session.commit()

    for _, row in forecast.iterrows():
        report_data = ReportData()
        report_data.report_id = report.id
        report_data.date = str(row.date)
        report_data.value = row.y
        report_data.sku = sku
        report_data.is_prediction = True
        db.session.add(report_data)
        db.session.commit()