from celery import Celery
from celery.decorators import periodic_task
from celery.schedules import crontab
from sqlalchemy import create_engine
from neodata.settings import db
from neodata.database import User, Project, Report, ReportData, ProphetSettings
from dotenv import load_dotenv
from datetime import timedelta
from fbprophet import Prophet
from sklearn.metrics import mean_squared_error
import numpy as np
import pandas as pd
import datetime
import calendar
import os
import itertools 
import tqdm
import json

load_dotenv()

celery_app = Celery(backend=os.environ.get("REDIS_URI"), 
                    broker=os.environ.get("REDIS_URI"))

def generate_project_reports():
    db.session.rollback()
    projects = Project.query.all()
    for _, project in enumerate(projects):
        user = User.query.filter_by(id=project.user_id).first()
        if not user:
            continue
        if not user.is_activated:
            continue
        print(f'processing {project.id}')
        #try:
        if True:
            _ = get_project_data(project, get_project_connection(project))
            #except:
        else:
            project.status = 'Não foi possível conectar com o banco de dados'
            db.session.commit()
            continue
        report = Report()
        report.done = False
        report.project_id = project.id
        report.date = datetime.datetime.now()
        report.public = False
        db.session.add(report)
        db.session.commit()
        for sku, real, forecast in run_forecasting(project):
            if not (sku):
                continue
            try:
                save_forecasting_to_database(report, sku, real, forecast)
                project.status = 'Dados processados com sucesso'
            except:
                project.status = 'Não foi possível conectar com o banco de dados'
                db.session.delete(report)
                db.session.commit()
                break
        else:
            project.status = 'Dados processados com sucesso'
            report.done = True
        db.session.commit()

    
def run_forecasting(project, settings=None):
    connection = get_project_connection(project)
    for sku, sku_data in get_project_data(project, connection):
        prophet_settings = ProphetSettings.query.filter_by(project_id=project.id, sku=sku).all()
        if prophet_settings:
            settings = json.loads(prophet_settings[-1].settings_json)
        yield sku, sku_data.tail(360), run_prophet(sku, sku_data, settings=settings)
    connection.dispose()
    del connection

def get_project_connection(project):
    if project.db_engine == 'mysql':
        connector_uri = 'mysql+pymysql'
    elif project.db_engine == 'postgres':
        connector_uri = 'postgresql+psycopg2'
    try:
        connection = create_engine(f'{connector_uri}://{project.db_user}:{project.db_pass}'
                                   f'@{project.db_host}/{project.db_name}')
        return connection
    except:
        return None

def get_project_data(project, connection):
    data = pd.read_sql(project.sql_query, connection)
    data.index=data['date']

    final_daily_df = pd.DataFrame()

    grouper = data['date'].dt.to_period('D')

    for p, df_period in data.groupby(by=grouper):
        for sku, df_sku in df_period.groupby(by='sku'):
            final_daily_df = final_daily_df.append({
                'date': p.end_time,
                'y': df_sku['y'].sum(),
                'sku': sku
            }, 
            ignore_index=True)

    final_daily_df['ds'] = final_daily_df['date']
    return final_daily_df.groupby(by="sku")

def run_prophet(sku, skus_data, settings=None):

    skus_data['ds'] = skus_data['date']
    skus_data['y'] = skus_data['y']

    group_df = skus_data[skus_data['sku']==sku]

    if not settings:
        m = Prophet()
    else:
        m = Prophet(**settings)

    m.fit(group_df.tail(360))
    future = m.make_future_dataframe(periods=31)
    forecast = m.predict(future)
    forecast['date'] = forecast['ds']
    forecast['y'] = forecast['yhat'].map(lambda v: v if v > 0 else 0)
    forecast['sku'] = sku    

    return forecast.tail(360)


def save_forecasting_to_database(report, sku, real, forecast):
    print(report)
    for _, row in real.iterrows():
        report_data = ReportData()
        report_data.report_id = report.id
        report_data.date = str(row.date)
        report_data.value = row.y
        report_data.sku = sku
        report_data.is_prediction = False
        db.session.add(report_data)
        db.session.commit()

    for _, row in forecast.iterrows():
        report_data = ReportData()
        report_data.report_id = report.id
        report_data.date = str(row.date)
        report_data.value = row.y
        report_data.sku = sku
        report_data.is_prediction = True
        db.session.add(report_data)
        db.session.commit()