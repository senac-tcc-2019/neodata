from flask import Flask
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, login_required
from flask_migrate import Migrate
from flask_admin import Admin
from flask_wtf.csrf import CSRFProtect
from dotenv import load_dotenv
import os
import stripe

load_dotenv()

stripe.api_key = os.environ.get('STRIPE_API_KEY_SECRET')

app = Flask(__name__, 
            template_folder='%s/templates/'%(os.getcwd()),
            static_folder='%s/static/'%(os.getcwd()),
            static_url_path='/static')

app.config['JSON_AS_ASCII'] = False
app.config['NEODATA_HOSTNAME'] = os.environ.get('NEODATA_HOSTNAME', 'localhost:5000')
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQLALCHEMY_DATABASE_URI')
app.config['SECRET_KEY'] = os.environ.get('FLASK_SECRET_KEY', '__')
app.config['MAIL_SERVER'] = os.environ.get('MAIL_SERVER')
app.config['MAIL_PORT'] = int(os.environ.get('MAIL_PORT', '587'))
app.config['MAIL_USERNAME'] = os.environ.get('MAIL_USERNAME')
app.config['MAIL_PASSWORD'] = os.environ.get('MAIL_PASSWORD')
app.config['MAIL_USE_TLS'] = True

app.jinja_env.globals.update(len=len)

mail = Mail(app)

db = SQLAlchemy(app)

from neodata.database import User, Project, Report, ReportData

migrate = Migrate(app, db)

login_manager = LoginManager()
login_manager.init_app(app)

from neodata.blueprints import account, projects, reports

app.register_blueprint(account)
app.register_blueprint(projects)
app.register_blueprint(reports)

csrf = CSRFProtect(app)

from neodata.admin import AdminView, userView, projectView, reportView, planView, subscriptionView, ProphetSettingsView, PasswordRecoveryTokenView

admin = Admin(app, name='admin', template_mode='bootstrap3', index_view=AdminView(User, db.session, url='/admin'))
admin.add_view(projectView)
admin.add_view(reportView)
admin.add_view(planView)
admin.add_view(subscriptionView)
admin.add_view(ProphetSettingsView)
admin.add_view(PasswordRecoveryTokenView)