
from flask import redirect, request, url_for
from flask_admin.contrib.sqla import ModelView
from flask_admin import expose, AdminIndexView
from neodata.settings import db
from flask_login import current_user
from neodata.database import User, Project, Report, Plan, Subscription, ProphetSettings, PasswordRecoveryToken

class BaseView(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated and current_user.is_admin

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('/', next=request.url))

class AdminView(ModelView):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.static_folder = 'static'
        self.endpoint = 'admin'

    def is_accessible(self):
        return current_user.is_admin

    def inaccessible_callback(self, name, **kwargs):
        if not self.is_accessible():
            return redirect('/')

userView = BaseView(User, db.session)
projectView = BaseView(Project, db.session)
planView = BaseView(Plan, db.session)
subscriptionView = BaseView(Subscription, db.session)
reportView = BaseView(Report, db.session)
ProphetSettingsView = BaseView(ProphetSettings, db.session)
PasswordRecoveryTokenView = BaseView(PasswordRecoveryToken, db.session)