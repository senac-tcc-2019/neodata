from neodata.settings import db
from flask_login import UserMixin


class User(db.Model, UserMixin):
    __table_args__ = {'extend_existing': True}
    __tablename__ = 'users'
    is_admin = db.Column(db.Boolean, default=False)
    is_activated = db.Column(db.Boolean, default=False)
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), unique=True, nullable=False)
    password = db.Column(db.String(100), unique=True, nullable=False)
    stripe_id =  db.Column(db.String(100), nullable=False)


class Project(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = 'projects'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    name = db.Column(db.String(100), nullable=False)
    db_name = db.Column(db.String(100), nullable=False)
    db_host = db.Column(db.String(100), nullable=False)
    db_user = db.Column(db.String(100), nullable=False)
    db_pass = db.Column(db.String(100), nullable=False)
    db_port = db.Column(db.Integer, nullable=False)
    db_engine = db.Column(db.String(100), nullable=False)
    sql_query = db.Column(db.String(1000), nullable=False)
    status = db.Column(db.String(100), nullable=False)
    reports = db.relationship("Report", backref="project", remote_side=[id], cascade="all,delete")


class Report(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = 'reports'
    id = db.Column(db.Integer, primary_key=True)
    done = db.Column(db.Boolean, nullable=False, default=False)
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id', ondelete='CASCADE'))
    date = db.Column(db.DateTime, nullable=False)
    data = db.relationship('ReportData', backref="report", remote_side=[id], cascade="all,delete")
    public = db.Column(db.Boolean, nullable=False)


class ReportData(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = 'reports_data'
    id = db.Column(db.Integer, primary_key=True)
    report_id = db.Column(db.Integer, db.ForeignKey('reports.id', ondelete='CASCADE'))
    date = db.Column(db.DateTime, nullable=False)
    is_prediction = db.Column(db.Boolean, nullable=False)
    sku = db.Column(db.String(100), nullable=False)
    value = db.Column(db.Float, nullable=False)

class Plan(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = 'plans'
    id = db.Column(db.Integer, primary_key=True)
    product_stripe_id = db.Column(db.String(100), nullable=False)
    plan_stripe_id = db.Column(db.String(100), nullable=False)
    period = db.Column(db.String(100), nullable=False)
    value = db.Column(db.Float, nullable=False)
    reports = db.relationship("Subscription", backref="plan", remote_side=[id])
    

class Subscription(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = 'subscriptions'
    id = db.Column(db.Integer, primary_key=True)
    stripe_id = db.Column(db.String(100), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    plan_id = db.Column(db.Integer, db.ForeignKey('plans.id'))
    started_at = db.Column(db.DateTime)
    finished_at = db.Column(db.DateTime)
    current_period_end = db.Column(db.DateTime)
    status = db.Column(db.String(100))
    


class PasswordRecoveryToken(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = 'passwordTokens'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    token = db.Column(db.String(100), nullable=False)
    valid = db.Column(db.Boolean, nullable=False)


class ProphetSettings(db.Model):
    __table_args__ = {'extend_existing': True}
    __tablename__ = 'prophet_settings'
    id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer)
    settings_json = db.Column(db.String(1000), nullable=False)
    rmse = db.Column(db.Float, nullable=False)
    sku = db.Column(db.String(100), nullable=False)


db.create_all()