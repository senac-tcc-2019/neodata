from neodata.database import ReportData
from neodata.settings import app
from flask import flash
from sqlalchemy import create_engine
import pandas as pd
import json

def reportData_to_dict_for_table(report_id):
    reportDataDict = [
        {
            'sku': report.sku,
            'date': report.date.strftime("%Y-%m-%d"),
            'real': (lambda x: x.value if x else None)(ReportData.query.filter_by(report_id=report_id, date=report.date, is_prediction=False).first()),
            'predicted': report.value,
        } for report in ReportData.query.filter_by(report_id=report_id, is_prediction=True).all()
    ]

    return reportDataDict

def reportData_to_dict_for_plot(report_id):
    reportDataDict = [
        {
            'sku': sku,
            'real': [
                {
                    'date' : reportData.date.strftime("%Y-%m-%d"),
                    'value': reportData.value
                }
                for reportData in ReportData.query.filter_by(report_id=report_id, is_prediction=False, sku=sku).all()
            ],
            'predicted': [
                {
                    'date' : reportData.date.strftime("%Y-%m-%d"),
                    'value': reportData.value
                }
                for reportData in ReportData.query.filter_by(report_id=report_id, is_prediction=True, sku=sku).all()
            ],
        } for sku in set(r.sku for r in ReportData.query.filter_by(report_id=report_id).all())
    ]
    return reportDataDict

def test_connect(user_id, db_name, db_user, db_pass, db_host, db_port, db_engine, sql_query):
    if db_engine == 'mysql':
        connector_uri = 'mysql+pymysql'
    elif db_engine == 'postgres':
        connector_uri = 'postgresql+psycopg2'
    try:
        connection = create_engine(f'{connector_uri}://{db_user}:{db_pass}'
                                   f'@{db_host}/{db_name}').connect()
        columns = pd.read_sql(sql_query, connection).columns
        if all([(field in columns) for field in ['sku', 'date', 'y']]):
            return True
        else:
            flash(f"A busca deve retornar os valos 'sku', 'date' e 'y'", category="error")
            return False
    except:
        flash(f"Não foi possível estabelecer conexão com o banco de dados", category="error")
        return False