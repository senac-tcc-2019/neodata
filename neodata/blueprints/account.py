from neodata.settings import app, mail, db, stripe
from neodata.database import User, Plan, Subscription, PasswordRecoveryToken
from neodata.forms import LoginForm, RegistrationForm
from neodata.mail.password_recovery import create_recovery_password_email_message
from flask_login import login_required, current_user, login_user, logout_user
from flask import Blueprint, redirect, render_template, flash, request, abort
from passlib.hash import pbkdf2_sha256
from sqlalchemy import update
from hashlib import md5
from datetime import datetime
import random

account = Blueprint("account", __name__, template_folder="templates")

@account.route("/login")
def login_page():
    if current_user.is_authenticated:
        return redirect('/')
    return render_template("login.html", static_url_path='/static')

@account.route("/register")
def register_page():
    if current_user.is_authenticated:
        return redirect("/")
    form = RegistrationForm()
    if current_user.is_authenticated and form.validate():
        return redirect('/')       
    return render_template("register.html", static_url_path='/static')

@account.route("/account")
@login_required
def account_page():
    user_stripe_data = stripe.Customer.retrieve(current_user.stripe_id)
    plans = Plan.query.all()
    subscriptions = Subscription.query.all()
    if user_stripe_data['default_source']:
        default_source = [source for source in user_stripe_data['sources']['data'] if source['id'] == user_stripe_data['default_source']][0]
    else:
        default_source = None
        flash('Um método de pagamento deve ser cadastrado', 'error')
    if not Subscription.query.filter_by(user_id=current_user.id, status="active").all():
        flash('Você não possui assinaturas ativas.', 'error')
    return render_template("account.html", static_url_path='/static', user_stripe_data=user_stripe_data, default_source=default_source, plans=plans, subscriptions=subscriptions)

@account.route("/registration", methods=['POST'])
def register():
    password = request.form.get('password')
    password_hash = pbkdf2_sha256.hash(password)
    stripe_id = stripe.Customer.create(email="fred.s.kremer@gmail.com", name=request.form.get('name', None)).id
    user = User(name=request.form.get('name', None), 
                email=request.form.get('email', None), 
                password=password_hash,
                stripe_id=stripe_id)
    
    try:
        db.session.add(user)
        db.session.commit()
        flash('Usuário cadastrado com sucesso', 'success')
    except:
        flash('Não foi possível afetuar o cadastro', 'error')

    return redirect("/register")

@account.route("/recovery")
def recovery_page():
    if current_user.is_authenticated:
        return redirect("/")
    if current_user.is_authenticated:
        return redirect('/')       
    return render_template("recovery.html", static_url_path='/static')

@account.route("/recovery/<string:token>", methods=['GET'])
def new_password_page(token):
    if current_user.is_authenticated:
        return redirect("/")
    if PasswordRecoveryToken.query.filter_by(token=token, valid=True).first():
        return render_template("password_change.html", static_url_path='/static', token=token)

@account.route("/recovery/<string:token>", methods=['POST'])
def new_password_update(token):
    if current_user.is_authenticated:
        return redirect("/")
    password_token = PasswordRecoveryToken.query.filter_by(token=token, valid=True).first()
    if not password_token:
        flash('Token de recuperação inválido', 'error')
        return redirect('/')

    password = request.form.get('password')
    password_hash = pbkdf2_sha256.hash(password)
    user = User.query.filter_by(id=password_token.user_id).first()
    user.password = password_hash
    db.session.commit()
    password_token.valid = False
    db.session.commit()
    flash('Senha atualizada com sucesso', 'success')
    return redirect("/login")

@account.route("/send_recovery_email", methods=['POST'])
def send_recovery_email():
    if current_user.is_authenticated:
        return redirect("/")
    form = RegistrationForm()
    if current_user.is_authenticated and form.validate():
        return redirect('/')
    email = request.form.get('email')
    user = User.query.filter_by(email=email).first()
    if not user:
        flash('Email não encontrado', 'error')
        return redirect('/recovery')
    else:
        token = PasswordRecoveryToken()
        token.user_id = user.id
        token.token = md5((str(random.random()) + str(user.id)).encode()).hexdigest()
        token.valid = True
        db.session.add(token)
        db.session.commit()
        message = create_recovery_password_email_message(user.name, user.email, token.token)
        mail.send(message)
        flash('Email de recuperação enviado', 'success')
        return redirect('/recovery')


@account.route("/auth", methods=['POST'])
def login():
    form = LoginForm()
    if not form.validate():
        return render_template("login.html", static_url_path='/static', form=form)

    email = request.form.get('email', None)
    password = request.form.get('password', None)
    user = User.query.filter_by(email=email).first()

    if user and pbkdf2_sha256.verify(password, user.password):
        login_user(user)
        return redirect("/")
    else:
        flash('Credenciais de acesso incorretas', 'error')
    return render_template("login.html", static_url_path='/static')

@account.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect('/')

@account.route("/delete_account", methods=['POST'])
@login_required
def delete_account():
    user_id = current_user.get_id()
    User.query.filter_by(id=user_id).delete()
    db.session.commit()
    logout_user()
    return redirect("/")

@account.route("/update_account", methods=['POST'])
@login_required
def update_account():
    password = request.form.get('password')
    new_password = request.form.get('new_password')
    new_password_hash = pbkdf2_sha256.hash(new_password)
    if pbkdf2_sha256.verify(password, current_user.password):
        User.query.filter_by(id=current_user.id).update(dict(password=new_password_hash))
        db.session.commit()
        flash('Senha atualizada com sucesso', 'success')
    else:
        flash('Senha atual incorreta', 'error')
    return redirect('/account')

@account.route("/charge", methods=['POST'])
@login_required
def save_card_data():
    stripe_token = request.form.get('stripeToken')
    user_stripe_id = current_user.stripe_id
    try:
        stripe.Customer.modify(
            user_stripe_id,
            source=stripe_token
        )
    except:
        flash('Problema ao cadastrar cartão. Tente novamente mais tarde. Se o problema persistir, contante nossa equipe (neodata@gmail.com)', 'error')
    return redirect('/account')

@account.route("/delete_default_card", methods=['POST'])
@login_required
def delete_default_card():
    user_stripe_id = current_user.stripe_id
    user_stripe_data = stripe.Customer.retrieve(user_stripe_id)
    try:
        stripe.Customer.delete_source(
            user_stripe_id,
            user_stripe_data['default_source']
        )
    except:
        flash('Problema ao remover cartão. Tente novamente mais tarde. Se o problema persistir, contante nossa equipe (neodata@gmail.com)', 'error')
    return redirect('/account')

@account.route("/create_subscription", methods=['POST'])
@login_required
def create_subscription():
    plan_id = request.form.get("plan", None)
    plan = Plan.query.filter_by(id=plan_id).first()
    customer_stripe_data = stripe.Customer.retrieve(current_user.stripe_id)
    if not plan:
        flash('Problema ao criar inscrição. Tente novamente mais tarde. Se o problema persistir, contante nossa equipe (neodata@gmail.com)', 'error')
        return redirect("/account")
    if not customer_stripe_data['default_source']:
        flash('Você precisa registrar um cartão de credito para assinar a conta. Se o problema persistir, contante nossa equipe (neodata@gmail.com)', 'error')
        return redirect("/account")
    subscription = Subscription()
    subscription.started_at = datetime.now()
    subscription.user_id = current_user.id
    subscription.plan_id = plan.id
    try:
        subscription_stripe_data = stripe.Subscription.create(
                                        customer=current_user.stripe_id,
                                        items=[
                                            {
                                                "plan":plan.plan_stripe_id
                                            }
                                        ]
                                    )
        subscription.stripe_id = subscription_stripe_data["id"]
        subscription.status = 'active'
        db.session.add(subscription)
        db.session.commit()
    except:
        flash('Problema ao criar inscrição. Tente novamente mais tarde. Se o problema persistir, contante nossa equipe (neodata@gmail.com)', 'error')
    return redirect('/account')

@account.route("/change_subscription", methods=['POST'])
@login_required
def change_subscription():
    customer_stripe_data = stripe.Customer.retrieve(current_user.stripe_id)
    if not customer_stripe_data['default_source']:
        flash('Você precisa registrar um cartão de credito para assinar a conta. Se o problema persistir, contante nossa equipe (neodata@gmail.com)', 'error')
        return redirect("/account")
    plan = Plan.query.filter_by(id=int(request.form.get('plan', 0))).first()
    for subscription in Subscription.query.filter_by(user_id=current_user.id).all():
        subscription.status = 'canceled'
        db.session.commit()
        try:
            stripe.Subscription.delete(
                subscription.stripe_id
            )
        except:
            pass
    if plan:
        subscription = Subscription()
        subscription.started_at = datetime.now()
        subscription.user_id = current_user.id
        subscription.plan_id = plan.id
        try:
            subscription.stripe_id = stripe.Subscription.create(customer=current_user.stripe_id, items=[{'plan': plan.plan_stripe_id}])['id']
            subscription.status = 'active'
            db.session.add(subscription)
            db.session.commit()
        except:
            flash('Problema ao alterar inscrição. Tente novamente mais tarde. Se o problema persistir, contante nossa equipe (neodata@gmail.com)', 'error')
    else:
        flash('Problema ao alterar inscrição. Tente novamente mais tarde. Se o problema persistir, contante nossa equipe (neodata@gmail.com)', 'error')

    return redirect('/account')

@account.route("/cancel_subscription", methods=['POST'])
@login_required
def cancel_subscription():
    subscription_id = int(request.form.get("subscription", 0))
    subscription = Subscription.query.filter_by(id=subscription_id, user_id=current_user.id).first()
    if subscription:
        try:
            stripe.Subscription.delete(subscription.stripe_id)
            subscription.status = 'canceled'
            subscription.finished_at = datetime.now()
        except:
            flash('Problema ao cancelar assinatura. Tente novamente mais tarde. Se o problema persistir, contante nossa equipe (neodata@gmail.com)', 'error')
        db.session.commit()
    else:
        flash('Problema ao cancelar assinatura. Tente novamente mais tarde. Se o problema persistir, contante nossa equipe (neodata@gmail.com)', 'error')
    return redirect('/account')