from neodata.settings import app, db
from neodata.database import Project, Report, ReportData
from neodata.utils import reportData_to_dict_for_plot, reportData_to_dict_for_table

from flask_login import login_required, current_user
from flask import Blueprint, redirect, render_template, flash, request, abort, jsonify
from functools import reduce
import json

reports = Blueprint("reports", __name__, template_folder="templates")

@reports.route("/reports", methods=['GET'])
@login_required
def reports_page():
    if not current_user.is_activated:
        return redirect("/account")
    user_projects = [project for project in Project.query.filter_by(user_id=current_user.id).all()]
    if user_projects:
        user_reports = list(reduce(lambda a,b: a + b, [list([report for report in project.reports if report.done]) for project in user_projects]))
    else:
        user_reports = []
    return render_template("reports.html", static_url_path='/static', reports=user_reports)


@reports.route("/reports/<int:report_id>", methods=['GET'])
@login_required
def report_page(report_id):
    if not current_user.is_activated:
        return redirect("/account")
    report = Report.query.filter_by(id=report_id).first()
    if report == None or report.project.user_id != current_user.id:
        return redirect('/reports')
    public_link = f'{app.config["NEODATA_HOSTNAME"]}/reports/{report_id}/public'
    print("public link:",public_link)
    return render_template("report.html", static_url_path='/static', from_public=False, report=report, public_link=public_link, report_data=reportData_to_dict_for_table(report_id), report_data_json=reportData_to_dict_for_plot(report_id), enumerate=enumerate)

@reports.route("/reports/<int:report_id>/public", methods=['GET'])
def report_page_public(report_id):
    report = Report.query.filter_by(id=report_id).first()
    if report and report.public:
        return render_template("report.html", static_url_path='/static', from_public=True, report=report, report_data=reportData_to_dict_for_table(report_id), report_data_json=reportData_to_dict_for_plot(report_id), enumerate=enumerate)
    else:
        return redirect("/")

@reports.route("/reports/<int:report_id>/data", methods=['GET'])
@login_required
def report_data(report_id):
    if not current_user.is_activated:
        return redirect("/account")
    report = Report.query.filter_by(id=report_id).first()
    if report == None or report.project.user_id != current_user.id:
        return redirect('/reports')
    else:
        return jsonify(reportData_to_dict_for_plot(report_id))

@reports.route("/reports/<int:report_id>/make_public", methods=['POST'])
@login_required
def make_public(report_id):
    if not current_user.is_activated:
        return redirect("/")
    report = Report.query.filter_by(id=report_id).first()
    if report.project.user_id != current_user.id:
        abort(400)
    report.public = not report.public
    db.session.commit()
    return redirect(f"/reports/{report_id}")
    
