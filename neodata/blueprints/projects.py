from neodata.settings import app, db
from neodata.database import Project
from neodata.forms import ProjectForm
from neodata.utils import test_connect
from flask_login import login_required, current_user
from flask import Blueprint, redirect, render_template, flash, request, abort

projects = Blueprint("projects", __name__, template_folder="templates")

@projects.route("/projects", methods=['GET'])
@login_required
def projects_page():
    if not current_user.is_activated:
        return redirect("/account")
    user_projects = Project.query.filter_by(user_id=current_user.id)
    return render_template("projects.html", static_url_path='/static', projects=user_projects)

@projects.route("/projects/new", methods=['GET'])
@login_required
def create_project_page():
    if not current_user.is_activated:
        return redirect("/account")
    return render_template("project.html", static_url_path='/static', project=None)

@projects.route("/projects", methods=['POST'])
@login_required
def create_projects():
    if not current_user.is_activated:
        return redirect("/account")
    form = ProjectForm()
    if form.validate_on_submit():
        if test_connect(user_id=current_user.id, db_name=form.db_name.data, db_user=form.db_user.data, db_pass=form.db_pass.data,
                        db_host=form.db_host.data, db_port=form.db_port.data, db_engine=form.db_engine.data, sql_query=form.sql_query.data):
            
            project  = Project(name=form.project_name.data,
                                user_id=current_user.id, db_name=form.db_name.data,
                                db_user=form.db_user.data, db_pass=form.db_pass.data,
                                db_host=form.db_host.data, db_port=form.db_port.data,
                                db_engine=form.db_engine.data, sql_query=form.sql_query.data,
                                status="queued")
            db.session.add(project)
            db.session.commit()
        return redirect("/projects/new")
    else:
        for field in form.errors:
            for error in form.errors[field]:
                flash(f"Field '{field}': '{error}'", category="error")
        return redirect("/projects/new")


@projects.route("/projects/<int:project_id>", methods=['GET'])
@login_required
def project_page(project_id):
    if not current_user.is_activated:
        return redirect("/account")
    project = Project.query.filter_by(id=project_id, user_id=current_user.id).first()
    if project == None:
        abort(400)
    else:
        return render_template("project.html", static_url_path='/static', project=project)

@projects.route("/projects/<int:project_id>/update", methods=['POST'])
@login_required
def update_project(project_id):
    if not current_user.is_activated:
        return redirect("/account")
    form = ProjectForm()
    project = Project.query.filter_by(id=project_id, user_id=current_user.id).first()
    if form.validate_on_submit() and project:
        if test_connect(user_id=current_user.id, db_name=form.db_name.data, db_user=form.db_user.data, db_pass=form.db_pass.data,
                        db_host=form.db_host.data, db_port=form.db_port.data, db_engine=form.db_engine.data, sql_query=form.sql_query.data):
            project.name = form.project_name.data
            project.db_name = form.db_name.data
            project.db_user = form.db_user.data
            project.db_pass = form.db_pass.data
            project.db_host = form.db_host.data
            project.db_engine = 'postgres'
            project.sql_query = form.sql_query.data
            project.status = "queued"
            db.session.commit()
            return redirect(f"/projects/{project.id}")
        else:
            return redirect(f"/projects/{project.id}")
    elif project:
        for field in form.errors:
            for error in form.errors[field]:
                flash(f"Field '{field}': '{error}'", category="error")
        return redirect(f"/projects/{project.id}")
    else:
        return redirect("/projects")


@projects.route("/projects/<int:project_id>/delete", methods=['POST'])
@login_required
def delete_project(project_id):
    if not current_user.is_activated:
        return redirect("/account")
    project = Project.query.filter_by(id=project_id).first()
    if project.user_id != current_user.id:
        abort(400)
    Project.query.filter_by(id=project_id).delete()
    db.session.commit()
    return redirect("/projects")

    
