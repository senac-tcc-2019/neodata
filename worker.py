from neodata.workers import *
from datetime import timedelta
from celery import Celery
from celery.decorators import periodic_task
import os
from dotenv import load_dotenv
load_dotenv()

celery_app = Celery(backend=os.environ.get("REDIS_URI"), 
                    broker=os.environ.get("REDIS_URI"))

@periodic_task(run_every=timedelta(days=1))
def wrapper_grid_search():
    grid_search()

@periodic_task(run_every=timedelta(days=1))
def wrapper_generate_project_reports():
    generate_project_reports()

@periodic_task(run_every=timedelta(minutes=1))
def wrapper_update_subscription_status():
    update_subscription_status()
